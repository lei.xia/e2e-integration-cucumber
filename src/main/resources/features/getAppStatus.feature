@ApiTest

Feature: the application status can be retrieved

  Scenario: client makes a GET request to /status
    When the client calls /status
    Then the client receives http status code of 200
    And the client receives application body 'OK'
