package uk.gov.dwp.health.bootful.e2e;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class E2eCucumberTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(E2eCucumberTestApplication.class, args);
	}

}
