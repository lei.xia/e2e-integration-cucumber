package uk.gov.dwp.health.bootful.e2e.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {

  @GetMapping(value = "/status")
  public String getStatus() {
    return "OK";
  }
}
