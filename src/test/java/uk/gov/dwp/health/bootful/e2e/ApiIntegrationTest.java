package uk.gov.dwp.health.bootful.e2e;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
    plugin = {"pretty", "html:target/cucumber-html", "json:target/report.json"},
    strict = true,
    monochrome = true,
    features = {"classpath:features"},
    tags = {"@ApiTest"})
public class ApiIntegrationTest {}
