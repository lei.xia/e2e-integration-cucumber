package uk.gov.dwp.health.bootful.e2e.cucumber;

import io.cucumber.java8.En;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppControllerStepDefs extends CucumberIntegrationAbstract implements En {

  @Autowired private TestRestTemplate template;
  private ResponseEntity<String> response;

  AppControllerStepDefs() {
    Before(() -> response = null);

    When(
        "the client calls /status",
        () -> response = template.getForEntity("/status", String.class));

    Then(
        "the client receives http status code of {int}",
        (Integer statusCode) -> assertThat(response.getStatusCode().value()).isEqualTo(statusCode));

    And(
        "the client receives application body {string}",
        (String body) -> assertThat(response.getBody()).isEqualTo(body));
  }

}
