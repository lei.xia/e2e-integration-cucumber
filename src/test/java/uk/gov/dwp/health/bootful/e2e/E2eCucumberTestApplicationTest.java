package uk.gov.dwp.health.bootful.e2e;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class E2eCucumberTestApplicationTest {

  @Autowired private ApplicationContext underTest;

  @Test
  void contextLoads() {
    assertThat(underTest).isNotNull();
  }
}
