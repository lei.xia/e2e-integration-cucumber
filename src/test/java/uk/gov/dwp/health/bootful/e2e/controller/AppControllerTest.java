package uk.gov.dwp.health.bootful.e2e.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class AppControllerTest {

  private AppController underTest;

  @BeforeEach
  public void setup() {
    underTest = new AppController();
  }

  @Test
  @DisplayName(value = "test invoke controller expect app status returned")
  public void shouldReturnVersionOfApplication() {
    String actual = underTest.getStatus();
    assertThat(actual).isEqualTo("OK");
  }
}
