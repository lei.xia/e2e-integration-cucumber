package uk.gov.dwp.health.bootful.e2e.cucumber;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import uk.gov.dwp.health.bootful.e2e.E2eCucumberTestApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles(value = {"test"})
@ContextConfiguration
@SpringBootTest(
    classes = E2eCucumberTestApplication.class,
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class CucumberIntegrationAbstract {}
